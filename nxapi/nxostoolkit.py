'''
Author: Dejan Tutnjevic
Date: December 2018

DESCRIPTION:
nexus class, using API REST to:
-authenticate to nexus switches
-retrieve the status from a specific interface
-configure the description of a specific interface

REQUIREMENTS:
-Python 3.6+
-needs Requests library installed in order to use GET and POST methods (http://docs.python-requests.org)
-NXAPI feature enabled on the nexus switch

NOTE:
the default constructor first asks at least for the nexus switch ip address,
but if the http NXAPI port has been changed, it is necesary to specify it.

[example >>>     switch = nexus('sbx-nxos-mgmt.cisco.com', '80') ].
'''

import requests
import json

class nexus:
    
    version = '9.2(1)'
    platform = 'Nexus9000 9000v Chassis'

    def __init__(self, ip, port = '80'):
        '''
        Default nexus constructor: takes ip address and http port as input.
        Sets default http port value to 80 if not specified.
        Both arguments have to be strings.
        '''
        
        self.address = {
            "ip": ip,
            "port": port
        }
        
    def authenticate(self, user, password):
        '''
        Authentication method: first argument is user, second argument is password.
        Both argument have to be strings.
        [example >>>     obj.authenticate('username', 'mypassword') ].
        '''
        
        auth_payload = {
            "aaaUser" : {
                "attributes" : {
                    "name": user,
                    "pwd": password
                }
            }
        }
        auth_uri = 'http://{}:{}/api/aaaLogin.json'.format(self.address['ip'],
                                                           self.address['port'])
        json_header = {'content-type': 'application/json'}
        auth_response = requests.post(auth_uri,
                                      json.dumps(auth_payload),
                                      json_header)
        
        auth_dict = auth_response.json()
        if auth_response.status_code == 200:
            print('Authentication to {} successful.'.format(self.address['ip']))
        else:
            print('Authentication failed')
            if auth_dict['imdata'][0]['error']['attributes']['code'] == '401':
                print('Wrong login credentials.')
            pass
        
        auth_token = auth_dict['imdata'][0]['aaaLogin']['attributes']['token']
        self.cookies = {'APIC-Cookie': auth_token}
        
    def get_interface_status(self, if_name):
        '''
        Get interface status method: argumnet is the ethernet interface name.
        The argument has to be a string.
        Accepted input values: "Eth*/*", "Ethernet*/*", "Ethernet */*"
        Returns the status as 'up', 'down' or 'unknown'
        [example >>>     obj.get_interface_status('eth1/1') ].
        '''
        
        stat_uri = 'http://{}:{}/api/mo/sys/intf/phys-[{}].json'.format(self.address['ip'],
                                                                        self.address['port'],
                                                                        if_name)
        stat_response = requests.get(stat_uri,
                                     cookies=self.cookies)

        stat_dict = stat_response.json()
        if stat_response.status_code == 200:
            stat_dict = stat_response.json()
            status = stat_dict['imdata'][0]['l1PhysIf']['attributes']['adminSt']
            print('The current {} interface status is: {}'.format(if_name, status))
            return status
        else:
            print('Failed to get the {} interface status. A problem occured.'.format(if_name))
            if stat_dict['imdata'][0]['error']['attributes']['code'] == '400':
                print('NOTE: Please make sure to input an acceptable interface name.')
                print('Acceptable names: "Eth*/*", "Ethernet*/*", "Ethernet */*"')
        
    def configure_interface_desc(self, if_name, description):
        '''
        Configure interface description method: first argumnet is interface name, second is descripiton.
        Both argument have to be strings.
        Accepted input interface name values: "Eth*/*", "Ethernet*/*", "Ethernet */*"
        [example >>>     obj.configure_interface_desc('eth1/1', 'description_example') ].
        '''

        desc_payload = {
            "l1PhysIf" : {
                "attributes" : {
                    "descr": description
                }
            }
        }
        desc_uri = 'http://{}:{}/api/mo/sys/intf/phys-[{}].json'.format(self.address['ip'],
                                                                     self.address['port'],
                                                                     if_name)
        desc_response = requests.post(desc_uri,
                                      json.dumps(desc_payload),
                                      cookies=self.cookies)

        if desc_response.status_code == 200:
            print('Interface {} description configured to: "{}"'.format(if_name, description))
        else:
            print('Failed to configure the {} interface description. A problem occured.'.format(if_name))
            desc_dict = desc_response.json()
            if desc_dict['imdata'][0]['error']['attributes']['code'] == '104':
                print('NOTE: Please make sure to input an acceptable interface name')
                print('Acceptable names: "Eth*/*", "Ethernet*/*", "Ethernet */*"')
